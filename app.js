// Load the http module to create an http server.
var finalhandler = require('finalhandler');
var http         = require('http');
var Router       = require('router');
var jade = require('jade');

var router = Router();

router.get('/', function (req, res) {
    res.end('Hello World!');
});

router.get('/hello', function (req, res) {
    var json = JSON.parse( '{"' + req._parsedUrl.query.replace(/=/img ,'":"').replace(/&/img ,'","') + '"}' );

    var html = jade.renderFile('index.jade', json);

    res.write(html);
    res.end();
});

router.get('/buy', function (req, res) {
    res.end('Hello World! buy');
});

// Configure our HTTP server to respond with Hello World to all reqs.
var server = http.createServer(function (req, res) {
    res.setHeader('Content-Type', 'text/html; charset=utf-8');

    router(req, res, finalhandler(req, res));

});


// Listen on port 8000, IP defaults to 127.0.0.1
server.listen(8000);

// Put a friendly message on the terminal
console.log("Server running at http://127.0.0.1:8000/");

